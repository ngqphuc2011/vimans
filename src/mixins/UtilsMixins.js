export default {
  data() {
    return {};
  },
  methods: {
    toTopPage() {
      document.body.scrollTop = 0;
      document.documentElement.scrollTop = 0;
    },
    compareObjects(a, b) {
      return JSON.stringify(a) === JSON.stringify(b);
    },
    formatPrice(num) {
      return num
        ? num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
        : "0";
    }
  }
};
