import Vue from "vue";
import Router from "vue-router";
import Homepage from "@/components/Homepage";
import OrderList from "@/components/orders/OrderList";
import OrderDetail from "@/components/orders/OrderDetail";
import UserProfile from "@/components/users/UserProfile";
import ProductList from "@/components/products/ProductList";
import ProductDetail from "@/components/products/ProductDetail";

Vue.use(Router);

const router = new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "Homepage",
      component: Homepage
    },
    {
      path: "/orders",
      name: "OrderList",
      component: OrderList
    },
    {
      path: "/orders/1",
      name: "OrderDetail",
      component: OrderDetail
    },
    {
      path: "/profile",
      name: "UserProfile",
      component: UserProfile
    },
    {
      path: "/products/id/:id",
      name: "ProductDetail",
      component: ProductDetail,
      props: true
    },
    {
      path: "/products/:category?",
      name: "ProductList",
      component: ProductList,
      props: true
    },
    {
      path: "/menus",
      name: "MenuList"
    }
  ]
});
router.afterEach((to, from) => {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
});

export default router;
