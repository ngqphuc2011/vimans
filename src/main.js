// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import App from "./App";
import router from "./router";
import vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";
import Vuetify from "vuetify";
import "@mdi/font/css/materialdesignicons.css";
import Axios from "axios";
import _ from "lodash";
import "./global.css";

Vue.config.productionTip = false;
Vue.use(vuetify);
Vue.prototype.$http = Axios;
Vue.prototype.baseUrl = process.env.PATH;

/* eslint-disable no-new */
new Vue({
  el: "#app",
  router,
  components: { App },
  vuetify: new Vuetify(),
  template: "<App/>"
});

export default new Vuetify({
  icons: {
    iconfont: "mdi" // default - only for display purposes
  }
});
